-- NOTE: This action is more or less copied from the formation package ...

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Moves all units uniformly to some edge (l, r, u, d)",
        parameterDefs = {
            {
                name = "edge",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "d",
            },
        }
    }
end

-- constants
local THRESHOLD_STEP = 100
local THRESHOLD_DEFAULT = 300

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
    self.threshold = THRESHOLD_DEFAULT
    self.lastPointmanPosition = Vec3(0, 0, 0)
end

local X_MIN = 0
local Z_MIN = 0
local X_MAX = Game.mapSizeX
local Z_MAX = Game.mapSizeZ


-- distributes units along given edge
local function ComputeFormation(cntUnits, edge)
    formation = {}
    for i = 1, cntUnits do
        if edge == "l" then
            -- left
            x = X_MIN
            z = (Z_MAX / cntUnits) * i - Z_MAX / cntUnits / 2
        elseif edge == "r" then
            -- right
            x = X_MAX
            z = (Z_MAX / cntUnits) * i - Z_MAX / cntUnits / 2
        elseif edge == "u" then
            -- up
            x = (X_MAX / cntUnits) * i - X_MAX / cntUnits / 2
            z = Z_MIN
        else
            -- down
            x = (X_MAX / cntUnits) * i - X_MAX / cntUnits / 2
            z = Z_MAX
        end
        formation[i] = Vec3(x, Spring.GetGroundHeight(x, z), z)
        -- Spring.Echo( "X: " .. x .. " Y: " ..  Spring.GetGroundHeight(x,z) .. " Z: " .. z )
    end

    return formation
end

local function isClose(self, unit, edge)
    local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
    if edge == "l" then
        -- left
        return math.abs(pointX - X_MIN) < self.threshold
    elseif edge == "r" then
        -- right
        return math.abs(pointX - X_MAX) < self.threshold
    elseif edge == "u" then
        -- up
        return math.abs(pointZ - Z_MIN) < self.threshold
    else
        -- down
        return math.abs(pointZ - Z_MAX) < self.threshold
    end
end

function Run(self, units, parameter)

    -- compute formation along edge
    formation = ComputeFormation(#units, parameter.edge)

    local pointman = units[1] -- while this is running, we know that #units > 0, so pointman is valid
    local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
    local pointmanPosition = Vec3(pointX, pointY, pointZ)

    -- prevent lock ...
    if (pointmanPosition == self.lastPointmanPosition) then
        self.threshold = self.threshold + THRESHOLD_STEP
    else
        self.threshold = THRESHOLD_DEFAULT
    end
    self.lastPointmanPosition = pointmanPosition

    -- check one guy success ...
    if (isClose(self, pointman, edge)) then
        return SUCCESS

        -- Move all units
    else
        for i = 1, #units do
            local thisUnitWantedPosition = formation[i]
            SpringGiveOrderToUnit(units[i], CMD.MOVE, thisUnitWantedPosition:AsSpringVector(), {})
        end
        return RUNNING
    end
end

function Reset(self)
    ClearState(self)
end
