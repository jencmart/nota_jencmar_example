-- NOTE: This action is more or less copied from the formation package ...

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Moves at least in single direction, if other not possible",
        parameterDefs = {
            {
                name = "center",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "formation", -- relative formation
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

-- constants
local THRESHOLD_STEP = 100
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
    self.threshold = THRESHOLD_DEFAULT
    self.lastPointmanPosition = Vec3(0, 0, 0)
end

function Run(self, units, parameter)
    local center = parameter.center -- Vec3
    local formation = parameter.formation -- array of Vec3

    local pointman = units[1] -- while this is running, we know that #units > 0, so pointman is valid
    local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
    local pointmanPosition = Vec3(pointX, pointY, pointZ)

    -- prevent lock ...
    if (pointmanPosition == self.lastPointmanPosition) then
        self.threshold = self.threshold + THRESHOLD_STEP
    else
        self.threshold = THRESHOLD_DEFAULT
    end
    self.lastPointmanPosition = pointmanPosition
    -- check one guy success ...
    if (pointmanPosition:Distance(center + formation[1]) < self.threshold) then
        return SUCCESS

        -- Move all units
    else
        for i = 1, #units do
            local thisUnitWantedPosition = center + formation[i]
            SpringGiveOrderToUnit(units[i], CMD.MOVE, thisUnitWantedPosition:AsSpringVector(), {})
        end
        return RUNNING
    end
end

function Reset(self)
    ClearState(self)
end
