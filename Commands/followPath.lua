function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move along given path",
        parameterDefs = {
            {
                name = "transportData",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "reverse",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "false",
            }
        }
    }
end

local THRESHOLD = 200

local function reverse_Path(initialized, givenList)
    local correct_path = {}
    for i = #givenList, 1, -1 do
        local tmp = givenList[i]
        table.insert(correct_path, tmp)
    end
    if not initialized then
        Spring.Echo("[info]: Reversing path.")
    end
    return correct_path
end

function Run(self, units, parameter)
    local vehicleID = parameter.transportData.vehicleID
    local path = parameter.transportData.path

    -- FAIL CONDITIONS
    if not Spring.ValidUnitID(vehicleID) then
        Spring.Echo("[fail]: unit is dead.")
        return FAILURE
    end
    if path == nil or #path == 0 then
        Spring.Echo("[fail]: path is broken.")
        return FAILURE
    end
    if  self.notMoved ~= nil and self.notMoved >= 15 then
        Spring.Echo("[fail]: unit stuck for too long.")
        global["blockedUnits"][vehicleID] = nil
        if not parameter.reverse then -- free the cargo only when we are unloaded
            global["blockedUnits"][parameter.transportData.cargoID] = nil
        end
        return FAILURE
    end

    -- Reverse Path if necessary
    if parameter.reverse then
        path = reverse_Path(self.initialized, path)
    end

    -- SUCCESS CONDITIONS
    local pointX, pointY, pointZ = Spring.GetUnitPosition(vehicleID)
    local unitPosition = Vec3(pointX, pointY, pointZ)
    target_pos = path[#path]
    if unitPosition:Distance(target_pos) < THRESHOLD then
        Spring.Echo("[success]: path destination reached.")
        return SUCCESS
    end

    -- first time
    if not self.initialized then
        self.notMoved = 0
        for _, poss in pairs(path) do
            pos = poss:AsSpringVector()
            local grid_x = pos[1]
            local grid_y = pos[2]
            local grid_z = pos[3]
            --- Vec3 -> Spring Vector !!!
            Spring.GiveOrderToUnit(vehicleID, CMD.MOVE, { grid_x, grid_y, grid_z }, { "shift" })
        end
        Spring.Echo("[info]: ordering path to destination.")
        self.initialized = true
    end

    -- check if stuck
    local uX, _, uZ = Spring.GetUnitPosition(vehicleID)
    local unitPosVec3 = Vec3(uX, 0, uZ)
    if (unitPosVec3 == self.lastUnitPosVec3) then
        self.notMoved = self.notMoved + 1
        --Spring.Echo("[warn]: unit stuck for: [" .. self.notMoved .. "].")
    end
    self.lastUnitPosVec3 = unitPosVec3

    return RUNNING
end

function Reset(self)
    self.initialized = false
    self.notMoved = 0
    self.lastUnitPosVec3 = Vec3(0, 0, 0)
end