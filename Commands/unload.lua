function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Unload a unit to given area",
        parameterDefs = {
            {
                name = "vehicleID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "targetArea",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

local THRESHOLD = 25

function is_close(UNIT_ID, targetPosition)
    local pointX, pointY, pointZ = Spring.GetUnitPosition(UNIT_ID)
    local unitPosition = Vec3(pointX, 0, pointZ)
    return unitPosition:Distance(targetPosition) < THRESHOLD

end

local function stopSliding(unitID)
    Spring.GiveOrderToUnit(unitID, CMD.STOP, {}, {})
    Spring.GiveOrderToUnit(unitID, CMD.STOP, {}, {})
    Spring.GiveOrderToUnit(unitID, CMD.WAIT, {}, {})
    Spring.GiveOrderToUnit(unitID, CMD.WAIT, {}, {})
    Spring.GiveOrderToUnit(unitID, CMD.STOP, {}, {})
    Spring.GiveOrderToUnit(unitID, CMD.WAIT, {}, {})
    Spring.GiveOrderToUnit(unitID, CMD.WAIT, {}, {})
    Spring.GiveOrderToUnit(unitID, CMD.STOP, {}, {})
end

local function getRandomNumber(a, b)
    math.randomseed(os.time() + os.time() + os.time())
    math.random();
    math.random(a, b);
    math.random(a, b);
    math.random(a, b);
    math.random(a % 3, 11 + b)
    return math.random(a, b)
end

function Run(self, units, parameter)
    local square_side = 150 -- (200-> 25 units) (100 -> 50 units)
    local safeArea = parameter.targetArea
    local safeAreaCenter = safeArea.center
    local sx = safeAreaCenter.x
    local sz = safeAreaCenter.z
    local radius = safeArea.radius
    local vehicleID = parameter.vehicleID

    if self.RESET_TIME ~= nil and self.RESET_TIME > 0 then
        self.RESET_TIME = self.RESET_TIME - 1
        if self.RESET_TIME == 0 then
            Reset(self)
            Spring.Echo("Resetting now.")
            return RUNNING
        end
        Spring.Echo("Resetting after [" .. self.RESET_TIME .. "].")
        return RUNNING
    end

    -- FAIL CONDITIONS
    if not Spring.ValidUnitID(vehicleID) then
        Spring.Echo("[fail]: vehicle is dead.")
        return FAILURE
    end

    -- SUCCESS CONDITIONS
    if #Spring.GetUnitIsTransporting(vehicleID) == 0 then
        if self.cargoID ~= nil then
            stopSliding(self.cargoID)
        end

        -- free units both units
        if vehicleID ~= nil then
            global["blockedUnits"][vehicleID] = nil
        end

        if self.cargoID ~= nil then
            global["blockedUnits"][self.cargoID] = nil
        end

        Spring.Echo("[success]: unit unloaded.")
        return SUCCESS
    end

    -- INIT
    if self.WAIT_TIME == nil then
        Spring.Echo("[info]: initializing unload.")
        self.cargoID = Spring.GetUnitIsTransporting(vehicleID)[1] -- the transported unit
        -- find free spot
        self.x, self.z = assign_rectangle_center(vehicleID, sx, sz, radius, square_side)
        -- move to free spot
        if self.x == nil or self.z == nil then
            Spring.Echo("[fail]: no space found, wait here forever.")
            -- do not free units
            return FAILURE
        else
            Spring.GiveOrderToUnit(vehicleID, CMD.MOVE, { self.x, 0, self.z }, {})
        end
        self.WAIT_TIME = 10  -- when hit 0 unload, when hit -20 fail
        self.MOVING_TIME = 0 -- when hit 20, it fails
    end

    -- STOP TIMER
    local targetPosition = Vec3(self.x, 0, self.z)
    if self.WAIT_TIME > 0 and is_close(vehicleID, targetPosition) then
        Spring.Echo("[info]: stopping timer: [" .. self.WAIT_TIME .. "].")
        self.WAIT_TIME = self.WAIT_TIME - 1
        Spring.GiveOrderToUnit(vehicleID, CMD.STOP, {}, {})
        Spring.Echo("[info]: stop ordered")
    elseif self.WAIT_TIME > 0 then
        self.MOVING_TIME = self.MOVING_TIME + 1
        Spring.Echo("[info]: going to unload position timer: [" .. self.MOVING_TIME .. "].")

    end

    -- UNLOAD TIMER
    if self.WAIT_TIME <= 0 then
        Spring.GiveOrderToUnit(vehicleID, CMD.UNLOAD_UNIT, { self.x, 0, self.z }, {})
        Spring.Echo("[info]: unloading timer: [" .. self.WAIT_TIME .. "].")
        self.WAIT_TIME = self.WAIT_TIME - 1
    end

    -- FAIL IF CAN'T UNLOAD
    if self.WAIT_TIME <= -20 or self.MOVING_TIME > 20 then
        Spring.Echo("[warn]: Start unload reset procedure.")
        self.RESET_TIME = getRandomNumber(3, 15)
        return RUNNING
    end

    return RUNNING
end

function Reset(self)
    self.WAIT_TIME = nil
    self.MOVING_TIME = nil
    self.RESET_TIME = nil
    self.cargoID = nil
end

-- 1200 diameter
-- square aaprox 800 x 800
-- 50 pcs .. we need about 8 x 8 positions

function assign_rectangle_center(UNIT_ID, Sx, Sz, Sr, square_side)
    square_uhlopricka = math.sqrt(2 * square_side * square_side)
    max_triangle = Sr ^ 2 - square_uhlopricka / 2 --ensure that resulting square will be in are (not only it's center)

    local free_rectangles = {}
    local cnt = 0
    -- traverse X .. rows
    for x_delta = -Sr, Sr, square_side do

        -- traverse Z .. cols
        for z_delta = -Sr, Sr, square_side do

            -- If inside safe zone ...
            if x_delta ^ 2 + z_delta ^ 2 < max_triangle then
                -- in area

                -- create the new indices
                local x_new = Sx + x_delta
                local z_new = Sz + z_delta

                -- check them ...
                if rectangle_with_center_is_free(UNIT_ID, x_new, z_new, square_side) then
                    cnt = cnt + 1
                    free_rectangles[cnt] = { x_new, z_new }
                end
            end
        end
    end

    if cnt == 0 then
        return nil, nil
    end

    local randomNum = getRandomNumber(1, cnt)
    local tmp = free_rectangles[randomNum]
    return tmp[1], tmp[2]
    --return nil, nil -- no free place in area
end

function rectangle_with_center_is_free(UNIT_ID, x, z, square_side)
    local x_min = x - square_side / 2
    local z_min = z - square_side / 2
    local x_max = x + square_side / 2
    local z_max = z + square_side / 2
    local unitsInRectangle = Spring.GetUnitsInRectangle(x_min, z_min, x_max, z_max)
    local Free = false
    if #unitsInRectangle == 0 then
        -- nobody in area
        Free = true
    elseif #unitsInRectangle == 1 and unitsInRectangle[1] == UNIT_ID then
        -- I am at the area
        Free = true
    elseif #unitsInRectangle == 2 then
        -- Me and my load in the area
        local loadID = Spring.GetUnitIsTransporting(UNIT_ID) --[1]
        if loadID == nil then
            return true
        end
        loadID = loadID[1]
        if unitsInRectangle[1] == UNIT_ID and unitsInRectangle[2] == loadID then
            Free = true
        elseif unitsInRectangle[2] == UNIT_ID and unitsInRectangle[1] == loadID then
            Free = true
        end
    end
    --Spring.Echo(string.format("Square Center [%f, %f], units: ", x, z) .. #unitsInRectangle .. " FREE: " .. tostring(Free))
    return Free
end