function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Load a given unit",
        parameterDefs = {
            {
                name = "vehicleID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "cargoID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

--[[
Node name: Load unit
Parameters
    unitID we would like to be rescued
    unitID of the transporter
Definition of SUCCESS: 
    “Unit to be rescued” is loaded in “transporter”
Definition of FAIL: 
    “Unit to be rescued” is dead
    “Unit to be rescued” is loaded by other trasnporter
    “Unit to be rescued” is not transportable
    “Transporter” is dead
    “Transporter” is not transporter
    “They are far away from each other”
    Unit stops to execute “Spring” order
Definition of RUNNING: 
    Not success, Not fail, 
Init (only first time): 
    Check all failure conditions
    Give “Spring” order to transporter to load a unit
Once running (always): 
    Check all failure conditions
    Check all success conditions
]]--

local function getUnitPositionVec3(vehicleID)
    local uX, uY, uZ = Spring.GetUnitPosition(vehicleID)
    return Vec3(uX, uY, uZ)
end

local function distanceBetweenUnits(unitID1, unitID2)
    vec3_1 = getUnitPositionVec3(unitID1)
    vec3_2 = getUnitPositionVec3(unitID2)
    return vec3_1:Distance(vec3_2)
end

function Run(self, units, parameter)

    -- FAIL CONDITIONS
    if not Spring.ValidUnitID(parameter.cargoID) then
        Spring.Echo("[fail]: cargo is dead.")
        return FAILURE
    end
    if not Spring.ValidUnitID(parameter.vehicleID) then
        Spring.Echo("[fail]: vehicle is dead.")
        return FAILURE
    end
    if distanceBetweenUnits(parameter.vehicleID, parameter.cargoID) > 2000 then
        Spring.Echo("[fail]: to far to load.")
        return FAILURE
    end

    -- SUCCESS CONDITIONS
    if Spring.GetUnitTransporter(parameter.cargoID) == parameter.vehicleID then
        Spring.Echo("[success]: unit loaded.")
        return SUCCESS
    end

    -- first time
    if not self.initialized then
        Spring.GiveOrderToUnit(parameter.vehicleID, CMD.LOAD_UNITS, { parameter.cargoID }, { "shift" })
        self.initialized = true
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end