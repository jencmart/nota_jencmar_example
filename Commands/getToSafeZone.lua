function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move along given paths",
        parameterDefs = {
            {
                name = "assgnments",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "safeZone",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

local THRESHOLD = 200

-- ASSIGN  N  TARGETS
-- COMPUTE N  PATHS
-- DRIVE TOWARD TARGETS (LOAD)
-- PICK N TARGETS
-- DRIVE TOWARD TARGETS (HOME)
-- UNLOAD N TARGETS

-- transporter ready to pick up -> pick up
-- transporter ready to return -> return
-- trasporter ready to unload -> unload
-- transporter unloaded -> remove him
-- transporter dead -> remove him
-- load dead -> remove him
-- nothing left ---> success

local STATE_GOING_TO = 1
local STATE_LOADING = 2
local STATE_GOING_BACK = 3
local STATE_UNLOADING = 4

local function orderPath(unitID, givenPath)
    for _, poss in pairs(givenPath) do
        pos = poss:AsSpringVector()
        local grid_x = pos[1]
        local grid_y = pos[2]
        local grid_z = pos[3]

        --- Vec3 -> Spring Vector !!!
        -- Spring.Echo("Giving order [" .. grid_x .. "," .. grid_y .. "," .. grid_z)
        Spring.GiveOrderToUnit(unitID, CMD.MOVE, { grid_x, grid_y, grid_z }, { "shift" })
    end
end

function Run(self, units, parameter)
    assignments = parameter.assgnments
    safe_zone = parameter.safeZone

    -- INIT == assign orderds to move
    if not self.initialized then
        for _, data in ipairs(assignments) do
            orderPath(data.unitID, data.path)
            self.queue[data.unitID] = 1
            self.queue_size = self.queue_size + 1
            self.initialized = true
        end
    end

    -- Check for those who are ready
    for _, data in ipairs(assignments) do
        local pointX, pointY, pointZ = Spring.GetUnitPosition(data.unitID)
        local unitPosition = Vec3(pointX, pointY, pointZ)
        target_pos = parameter.path[#data.path]
        if unitPosition:Distance(target_pos) < THRESHOLD then
            self.queue_size = self.queue_size - 1
            self.queue[data.unitID] = nil
        end
    end

    -- success when all finished
    if self.queue_size == 0 then
        return SUCCESS
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
    self.queue = {}
    self.queue_size = 0
end