-- NOTE: This action is more or less copied from the formation package ...

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move around edges smoothly. Don't get stuck",
        parameterDefs = {
            {
                name = "targetpos",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "fight",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "false",
            }
        }
    }
end

-- constants
local EDGE_THRESHOLD = 50    -- do not get closer to the edge that this value
local THRESHOLD_STEP = 100   -- step for threshold when stuck
local THRESHOLD_DEFAULT = 10 -- success when this close to the target

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
    local targetPosition = parameter.targetpos
    local fight = parameter.fight -- boolean

    local mapX = Game.mapSizeX -- / Game.squareSize
    local mapZ = Game.mapSizeZ --  / Game.squareSize

    -- fix the X coord
    if (targetPosition.x < EDGE_THRESHOLD) then
        targetPosition = Vec3(EDGE_THRESHOLD, targetPosition.y, targetPosition.z)
        Spring.Echo("Correct left edge ( x < 0 )")

    elseif ((mapX - targetPosition.x) < EDGE_THRESHOLD) then
        targetPosition = Vec3(mapX - EDGE_THRESHOLD, targetPosition.y, targetPosition.z)
        Spring.Echo("Correct right edge ( x > mapSize.X )")
    end

    -- fix the Z coord
    if (targetPosition.z < EDGE_THRESHOLD) then
        targetPosition = Vec3(targetPosition.x, targetPosition.y, EDGE_THRESHOLD)
        Spring.Echo("Correct top edge ( z < 0 )")

    elseif ((mapZ - targetPosition.z) < EDGE_THRESHOLD) then
        targetPosition = Vec3(targetPosition.x, targetPosition.y, mapZ - EDGE_THRESHOLD)
        Spring.Echo("Correct bottom edge ( z > mapSize.Z )")
    end


    -- while this is running, we know that #units > 0
    local uX, _, uZ = SpringGetUnitPosition(units[1])
    local unitPosVec3 = Vec3(uX, 0, uZ)

    if (unitPosVec3:Distance(targetPosition) < self.threshold) then
        self.threshold = THRESHOLD_DEFAULT
        return SUCCESS
    end

    -- Enlarge threshold, if unit stuck (otherwise reset threshold)
    if (unitPosVec3 == self.lastUnitPosVec3) then
        self.threshold = self.threshold + THRESHOLD_STEP
        Spring.Echo("Updating treshold")
    else
        self.threshold = THRESHOLD_DEFAULT
    end

    -- if success, return
    if (unitPosVec3:Distance(targetPosition) < self.threshold) then
        self.threshold = THRESHOLD_DEFAULT
        return SUCCESS
        -- else move towards goal
    else
        self.lastUnitPosVec3 = unitPosVec3
        for i = 1, #units do
            SpringGiveOrderToUnit(units[i], CMD.MOVE, targetPosition:AsSpringVector(), {})
        end
        return RUNNING
    end
end

function Reset(self)
    self.threshold = THRESHOLD_DEFAULT
    self.lastUnitPosVec3 = Vec3(0, 0, 0)
end
