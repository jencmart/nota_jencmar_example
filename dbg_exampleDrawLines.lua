moduleInfo = {
	name = "demoVisualDebugDrawingWidget",
	desc = "Example lines drawer",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "notAlicense",
	layer = -1,
	enabled = true
}

function widget:GetInfo()
	return moduleInfo
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "stringExt")
Vec3 = attach.Module(modules, "vec3")

local spEcho = Spring.Echo
local spAssignMouseCursor = Spring.AssignMouseCursor
local spSetMouseCursor = Spring.SetMouseCursor
local spGetGroundHeight = Spring.GetGroundHeight
local spTraceScreenRay = Spring.TraceScreenRay
local spGetUnitPosition = Spring.GetUnitPosition
local glColor = gl.Color
local glRect = gl.Rect
local glTexture	= gl.Texture
local glDepthTest = gl.DepthTest
local glBeginEnd = gl.BeginEnd
local glPushMatrix = gl.PushMatrix
local glPopMatrix = gl.PopMatrix
local glTranslate = gl.Translate
local glText = gl.Text
local glLineWidth = gl.LineWidth
local glLineStipple = gl.LineStipple
local glVertex = gl.Vertex
local GL_LINE_STRIP = GL.LINE_STRIP
local TextDraw = fontHandler.Draw
local max = math.max
local min = math.min

local instances = {}

-- Draw Line Function
--local function Line(a, b)
--    glVertex(a[1], a[2], a[3])
--    glVertex(b[1], b[2], b[3])
--end
--local function DrawLine(a, b)
--    glLineStipple(false)
--    glLineWidth(5)
--    glBeginEnd(GL_LINE_STRIP, Line, a, b)
--    glLineStipple(false)
--end
-- glColor(1, 0, 0, 0.2)
-- two arrays of 3 numbers, Spring notation of Vec3
-- DrawLine({x,y,z}, endPosition:AsSpringVector())
-- glColor(1, 0, 0, 1)

-- Draw Icon Function
--function DrawIcon(name, orderX, orderY, orderZ, sizeX, sizeZ)
--   	gl.PushMatrix()
--   	gl.Texture(name)
--   	gl.Translate(orderX-sizeX/2, orderY+10, orderZ+sizeZ/2)
--   	gl.Billboard()
--   	gl.TexRect(sizeX, sizeZ, 0, 0, true, true)
--   	gl.PopMatrix()
--end
-- glColor(1, 0, 0, 0.6)	
-- DrawIcon(":n:" .. iconPath, orderX, orderY, orderZ, 64, 64)
-- glColor(1, 0, 0, 1)


-- Update will store the line data
local function Update(lineID, lineData)
	instances[lineID] = lineData
end

function widget:Initialize() -- we registar that "exampleDebug_update" will call Update(lineID, lineData)
	widgetHandler:RegisterGlobal('exampleDebug_update', Update)
end

function widget:GameFrame(n)
end

function widget:DrawWorld()
	for instanceKey, instanceData in pairs(instances) do		
		if (instanceData.startPos ~= nil and instanceData.endPos ~= nil) then
			local function Line(a, b)
				glVertex(a[1], a[2], a[3])
				glVertex(b[1], b[2], b[3])
			end
			
			line_width = 5
			
			if instanceData.line_width ~= nil then
				line_width = instanceData.line_width
			end
			
			local function DrawLine(a, b)
				glLineStipple(false)
				glLineWidth( line_width )
				glBeginEnd(GL_LINE_STRIP, Line, a, b)
				glLineStipple(false)
			end
		
			if (instanceData.color ~= nil) then
				glColor(instanceData.color[1] ,instanceData.color[2], instanceData.color[3], instanceData.color[4] )
			else
				glColor(1, 0, 0, 0.2)
			end
		
			
			DrawLine(
				{
					instanceData.startPos.x,
					instanceData.startPos.y,
					instanceData.startPos.z
				}, 
				{
					instanceData.endPos.x,
					instanceData.endPos.y,
					instanceData.endPos.z
				}
			)
		end
	end
	glColor(1, 0, 0, 1)
end







