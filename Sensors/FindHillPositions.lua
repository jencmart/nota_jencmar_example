local sensorInfo = {
    name = "HillPositions",
    desc = "Finds all hills with given height.",
    author = "jencmar",
    date = "2021-06-04",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local sin, cos = math.sin, math.cos

function left(x, z, hillHeight)
    minX = x
    while (Spring.GetGroundHeight(x, z) == hillHeight) do
        x = x - 1
        minX = x
    end
    return minX
end

function right(x, z, hillHeight)
    maxX = x
    while (Spring.GetGroundHeight(x, z) == hillHeight) do
        x = x + 1
        maxX = x
    end
    return maxX
end

function top(x, z, hillHeight)
    minY = z
    while (Spring.GetGroundHeight(x, z) == hillHeight) do
        z = z - 1
        minY = z
    end
    return minY
end

function bottom(x, z, hillHeight)
    maxY = z
    while (Spring.GetGroundHeight(x, z) > hillHeight) do
        z = z + 1
        maxY = z
    end
    return maxY
end

function findHillCenter(x, z, hillHeight)
    x1 = left(x, z, hillHeight)
    x2 = right(x, z, hillHeight)
    y1 = top(x, z, hillHeight)
    y2 = bottom(x, z, hillHeight)
    return Vec3(x2 - x1, hillHeight, y2 - y1)
end

local X_MIN = 1024
local Z_MIN = 0
local X_MAX = Game.mapSizeX
local Z_MAX = Game.mapSizeZ
local STEP = 128

return function(hillHeight)
    hillTable = {}

    -- width
    for x = X_MIN, X_MAX, STEP do
        -- height
        for z = Z_MIN, Z_MAX, STEP do
            -- this is hill
            y = Spring.GetGroundHeight(x, z)
            if (y == hillHeight) then
                -- Vec3
                -- hillCenter = findHillCenter(x, z, hillHeight)
                hillTable[#hillTable + 1] = Vec3(x, y, z)
            end
        end
    end

    Spring.Echo("Found hills:= [" .. #hillTable .. "].")
    return hillTable
end