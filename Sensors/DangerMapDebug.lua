local sensorInfo = {
    name = "DangerMapDebug",
    desc = "Sends data debug widget",
    author = "jencmar",
    date = "2021-06-04",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local function computeGridStep(dm)
    items = 0
    for _, _ in pairs(dm) do
        items = items + 1
    end
    Spring.Echo("ITEMS" .. items)
    return Game.mapSizeX / (items - 1)
end

return function(dangerMap)

    local STEP = computeGridStep(dangerMap)

    local idx = 0

    -- row
    for grid_x = 0, 8192, STEP do

        -- col -- we draw small columns ...
        for grid_z = 0, 8192, STEP do
            if (dangerMap[grid_x][grid_z] == 0) then
                c = { 1, 0, 0 } --  red
            elseif dangerMap[grid_x][grid_z] == 1 then
                c = { 0.2, 0.9, 0.1 } -- green
            else
                c = { 1, 1, 0.3 } -- yellow
            end
            idx = idx + 1

            z_min = math.max(grid_z - STEP / 8, 0)
            z_max = math.min(grid_z + STEP / 8, Game.mapSizeZ)
            local y = Spring.GetGroundHeight(grid_x, z_min)

            Script.LuaUI.exampleDebug_update(idx, -- key
                    {        -- data
                        startPos = Vec3(grid_x, y, z_min),
                        endPos = Vec3(grid_x, y, z_max),
                        color = c,
                        line_width = STEP / 8
                    }
            )


        end
    end


end