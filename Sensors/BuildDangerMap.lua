local sensorInfo = {
    name = "BuildDangerMap",
    desc = "Creates danger map from the known enemy positions.",
    author = "jencmar",
    date = "2021-06-04",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local X_MIN = 0
local Z_MIN = 0
local X_MAX = Game.mapSizeX  -- 8192
local Z_MAX = Game.mapSizeZ  -- 8192
local STEP = 64
local DANGER = 0
local SAFE = 1

local function isSafe(x, z, dangerMap)
    if dangerMap[x] == nil or dangerMap[x][z] == nil then
        -- Spring.Echo("Position Does Not Exist: [" .. x .. "," .. z .. "]")
        return false
    end
    return true
end

local function maxHeightAround(x, z, dm)
    local height = Spring.GetGroundHeight(x, z)

    -- left 
    if isSafe(x - STEP, z, dm) then
        height = math.max(height, Spring.GetGroundHeight(x - STEP, z))
    end

    -- rigt
    if isSafe(x + STEP, z, dm) then
        height = math.max(height, Spring.GetGroundHeight(x + STEP, z))
    end

    -- up
    if isSafe(x, z - STEP, dm) then
        height = math.max(height, Spring.GetGroundHeight(x, z - STEP))
    end

    -- down
    if isSafe(x, z + STEP, dm) then
        height = math.max(height, Spring.GetGroundHeight(x, z + STEP))
    end

    -- left up
    if isSafe(x - STEP, z - STEP, dm) then
        height = math.max(height, Spring.GetGroundHeight(x - STEP, z - STEP))
    end

    -- rigt down
    if isSafe(x + STEP, z + STEP, dm) then
        height = math.max(height, Spring.GetGroundHeight(x + STEP, z + STEP))
    end


    -- right up
    if isSafe(x + STEP, z - STEP, dm) then
        height = math.max(height, Spring.GetGroundHeight(x + STEP, z - STEP))
    end


    -- left down
    if isSafe(x - STEP, z + STEP, dm) then
        height = math.max(height, Spring.GetGroundHeight(x - STEP, z + STEP))
    end

    return height
end

local function isInLOS(vecA, vecB)

    if math.abs(vecA.y - vecB.y) < 250 then
        -- must be at least this apart
        return true
    else
        return false
    end

    --local vecC = vecB - vecA -- between enemy and me (z A do B)
    --local HIGHER_THAN_CONST = 150

    --local plane_dist = math.sqrt( vecC.x * vecC.x  + vecC.z * vecC.z )
    --for i = 1, plane_dist, STEP do
    --	local norm_factor = i / plane_dist -- 1 / 100 , 64 / 100,
    --	local los_height = vecA.y + vecC.y * norm_factor
    --	local true_height = Spring.GetGroundHeight(  vecC.x * norm_factor,  vecC.z * norm_factor )
    --	Spring.Echo("los: " .. los_height .. " true: " .. true_height )
    --	if math.abs(los_height - true_height) > HIGHER_THAN_CONST  then
    --		Spring.Echo("NOT VSIBLE from:[" .. vecA.x .. "," ..    vecA.y .. ","  .. vecA.z .. "] to:[" .. vecB.x .. ","       ..    vecB.y .. ","     .. vecB.z .. "]")
    --		Spring.Echo("--------------------")
    --		return false
    ----    end
    --end
    --Spring.Echo("--------------------")
    --return true

end

local function getFlyHeight(grid_x, grid_z, dm)
    fly_height = 0
    local tmp = maxHeightAround(grid_x, grid_z, dm) -- Spring.GetGroundHeight( grid_x, grid_z ) --
    local res = tmp + fly_height
    return res
end

local function initDangerMap()
    local dangerMap = {}
    for i = X_MIN, X_MAX, STEP do
        dangerMap[i] = {}
        for j = Z_MIN, Z_MAX, STEP do
            dangerMap[i][j] = SAFE
        end
    end
    return dangerMap
end

local function transformEnemyInfo(enemyInfo)
    local transformedEnemyData = {}
    transformedEnemyData["enemyDataPosition"] = Vec3(enemyInfo[1], enemyInfo[2], enemyInfo[3])
    transformedEnemyData["enemyRange"] = enemyInfo[4]
    transformedEnemyData["enemyHeight"] = enemyInfo[5]
    transformedEnemyData["enemyLos"] = enemyInfo[6]
    transformedEnemyData["enemyAirLos"] = enemyInfo[7]
    transformedEnemyData["name"] = enemyInfo[8]

    --if(transformedEnemyData["enemyRange"] == nil) then
    --	transformedEnemyData["enemyRange"]  = 700
    --	transformedEnemyData["enemyHeight"] = 50
    --	transformedEnemyData["enemyLos"]  = 650
    --	transformedEnemyData["enemyAirLos"] = 800
    --end
    return transformedEnemyData
end

local function tableLen(T)
    if T == nil then
        return 0
    end
    local count = 0
    for _, val in pairs(T) do
        if (type(val) == "table") then
            count = count + 1
        end
    end
    return count
end

local function removeAndGetFromTable(T)
    for key, val in pairs(T) do
        if (type(val) == "table") then
            T[key] = nil
            return val
        end
    end
    return nil
end

local function analyzeForSingleUnit(listOfEnemyInfo, dangerMap)
    -- Get single enemy
    local tmp = removeAndGetFromTable(listOfEnemyInfo)

    -- Done
    if tmp == nil then
        return { nil, dangerMap }
    end

    local enemyData = transformEnemyInfo(tmp)
    local enemyPos = enemyData["enemyDataPosition"]

    -- to speedup the computation, consider only some smaller square to search for dangerous unit
    -- we consider only grid (-2000, +2000) from the enemy
    -- but we need to align it with the grid ( subtract the difference from grid )
    local rect_half = 2000
    local smaller_X_MIN = math.max(X_MIN                    , (enemyPos.x - rect_half) - ((enemyPos.x - rect_half)%STEP))
    local smaller_X_MAX = math.min(X_MAX - (X_MAX%STEP) , (enemyPos.x + rect_half) - ((enemyPos.x + rect_half)%STEP))
    local smaller_Z_MIN = math.max(Z_MIN                    , (enemyPos.z - rect_half) - ((enemyPos.z - rect_half)%STEP))
    local smaller_Z_MAX = math.min(Z_MAX - (Z_MAX%STEP) , (enemyPos.z + rect_half) - ((enemyPos.z + rect_half)%STEP))

    -- For each grid position
    for grid_x = smaller_X_MIN, smaller_X_MAX, STEP do
        for grid_z = smaller_Z_MIN, smaller_Z_MAX, STEP do
            --if dangerMap[grid_x][grid_z] == nil then
            --    Logger.warn("NIL ON GRID")
            --end
            -- consider only safe positions...
            if dangerMap[grid_x][grid_z] == SAFE then

                -- over estimate your height ( max from grid tiles around you )
                local gridPosition = Vec3(grid_x, getFlyHeight(grid_x, grid_z, dangerMap), grid_z)

                local los = enemyData["enemyLos"]
                local airLos = enemyData["enemyAirLos"]

                -- if it is crasher, consider it looks other direction (if you are more in the botom of map)
                if enemyData["name"] ~= nil and string.find(enemyData["name"], "Crasher") then
                    if gridPosition.z - enemyPos.z > 100 then
                        --Spring.Echo("Reducing Distance")
                        airLos = los
                    end
                end

                -- if below enemy < 300 then LOS, else AIR LOS
                local dangerRadius = enemyPos.y - gridPosition.y > 300 and los or airLos

                -- distance_from_enemy
                local distance_from_enemy = gridPosition:Distance(enemyPos) - 50 -- you always fly a bit closer...

                -- If close && in LOS
                if distance_from_enemy < dangerRadius and isInLOS(gridPosition, enemyPos) then
                    if (grid_x == 5120 and grid_z == 6912) then
                        Spring.Echo("THE POSITION")
                        Spring.Echo(enemyData["name"])
                        Spring.Echo(gridPosition.x)
                        Spring.Echo(gridPosition.z)
                        Spring.Echo(distance_from_enemy)
                        Spring.Echo(dangerRadius)
                    end
                    dangerMap[grid_x][grid_z] = DANGER
                end
            end
        end
    end

    return { listOfEnemyInfo, dangerMap }
end

return function(dataIn)
    local listOfEnemyInfo = dataIn[1]
    local dangerMap = dataIn[2]

    -- Init new dangerMap
    if dangerMap == nil then
        dangerMap = initDangerMap()
        Spring.Echo("Remaining:  [" .. tableLen(listOfEnemyInfo) .. "].")
        return { listOfEnemyInfo, dangerMap }
    end

    -- analyze for 10 units is fine
    for _ = 1, 25 do -- 20 == 8 kroku ; 25 == 6 krokru`
        -- Done
        if listOfEnemyInfo == nil then
            Spring.Echo("Remaining:  [0].")
            return { nil, dangerMap }
        end

        -- analyze for single unit
        local xx = analyzeForSingleUnit(listOfEnemyInfo, dangerMap)
        listOfEnemyInfo = xx[1]
        dangerMap = xx[2]
    end

    Spring.Echo("Remaining:  [" .. tableLen(listOfEnemyInfo) .. "].")
    return { listOfEnemyInfo, dangerMap }
end

