local sensorInfo = {
    name = "FindSafePath",
    desc = "Finds save path between two points.",
    author = "jencmar",
    date = "2021-06-04",
    license = "notAlicense",
}

--------- Q U E U E -----------------------------------------------------
function pushleft(list, value)
    local first = list.first - 1
    list.first = first
    list[first] = value
end

function popright (list)
    local last = list.last
    if list.first > last then
        error("list is empty")
    end
    local value = list[last]
    list[last] = nil         -- to allow garbage collection
    list.last = last - 1
    return value
end

function is_empty(list)
    return list.first > list.last
end

---------  H A S H ---------------------------------------------------
local function hashNode(v)
    return v[1] * Game.mapSizeX + v[2]
end

local function computeGridStep(dm)
    items = 0
    for _, _ in pairs(dm) do
        items = items + 1
    end
    --Spring.Echo("ITEMS" .. items)
    return Game.mapSizeX / (items - 1)
end

--------- I N I T - N O D E S -------------------------------------------------

local function prepareNodes(startPos, endPos, dangerMap, STEP)
    Spring.Echo("[info]: finding path: [" .. math.floor(startPos[1])  .. ", " .. math.floor(startPos[2]) .. "]  --> [" .. math.floor(endPos[1])  .. ", " .. math.floor(endPos[2]) .. "]")
    local start_node = { math.floor(startPos[1] / STEP + 0.5) * STEP, math.floor(startPos[2] / STEP + 0.5) * STEP }
    local end_node = { math.floor(endPos[1] / STEP + 0.5) * STEP, math.floor(endPos[2] / STEP + 0.5) * STEP }
    --Spring.Echo("Corrected from: [" .. start_node[1] .. "," .. start_node[2] .. "]")
    --Spring.Echo("Corrected to:   [" .. end_node[1] .. "," .. end_node[2] .. "]")
    --Spring.Echo("Corrected start on map: " .. dangerMap[start_node[1]][start_node[2]])
    --Spring.Echo("Corrected end on map:   " .. dangerMap[end_node[1]][end_node[2]])
    return { start_node, end_node }
end

--------- N E I G H B O R S ---------------------------------------------------
local function isSafe(x, z, dangerMap)
    if dangerMap[x] == nil or dangerMap[x][z] == nil then
        -- Spring.Echo("Position Does Not Exist: [" .. x .. "," .. z .. "]")
        return false
    end
    if dangerMap[x][z] == 1 then
        --  Spring.Echo("Position Ok: [" .. x .. "," .. z .. "]")
        return true
    end
    -- Spring.Echo("Position Not Safe: [" .. x .. "," .. z .. "]")
    return false
end

local function getNeighbours(v, dm, STEP)
    local neighbors = {}
    local x = v[1]
    local z = v[2]

    -- left
    if isSafe(x - STEP, z, dm) then
        table.insert(neighbors, { x - STEP, z })
    end

    -- rigt
    if isSafe(x + STEP, z, dm) then
        table.insert(neighbors, { x + STEP, z })
    end


    -- up
    if isSafe(x, z - STEP, dm) then
        table.insert(neighbors, { x, z - STEP })
    end

    -- down
    if isSafe(x, z + STEP, dm) then
        table.insert(neighbors, { x, z + STEP })
    end

    -- left up
    if isSafe(x - STEP, z - STEP, dm) then
        table.insert(neighbors, { x - STEP, z - STEP })
    end

    -- rigt down
    if isSafe(x + STEP, z + STEP, dm) then
        table.insert(neighbors, { x + STEP, z + STEP })
    end


    -- right up
    if isSafe(x + STEP, z - STEP, dm) then
        table.insert(neighbors, { x + STEP, z - STEP })
    end


    -- left down
    if isSafe(x - STEP, z + STEP, dm) then
        table.insert(neighbors, { x - STEP, z + STEP })
    end

    return neighbors
end

--------- R E C O N S T R U C T    P A T H ---------------------------------
local function reconstruct_path(predacessors, start_node, end_node)
    local final_path = {}
    local curr_node = end_node

    -- build path from back...
    while (true) do
        -- Spring.Echo("Inserting Backward [" .. curr_node[1] .. "," .. curr_node[2] .. "]")
        table.insert(final_path, curr_node)
        if curr_node[1] == start_node[1] and curr_node[2] == start_node[2] then
            break
        end
        curr_node = predacessors[hashNode(curr_node)]
    end

    -- reverse path  ...
    local correct_path = {}
    for i = #final_path, 1, -1 do
        -- Spring.Echo("Reversing [" .. final_path[i][1] .. "," .. final_path[i][2] .. "]")
        local tmp = final_path[i]
        table.insert(correct_path, Vec3(tmp[1], Spring.GetGroundHeight(tmp[1], tmp[2]), tmp[2]))
    end
    return correct_path
end

---------  R U N - A L G O ------------------------------------------------
local function find_single_path(dangerMap, a, b)

    local step_size = computeGridStep(dangerMap)

    -- Init
    res = prepareNodes(a, b, dangerMap, step_size)
    local start_node = res[1]
    local end_node = res[2]
    local visited = {}
    local predacessors = {}
    local Q = { first = 0, last = -1 }
    visited[hashNode(start_node)] = 1  -- mark s as visited
    pushleft(Q, start_node) --table.insert(Q, start_node) -- Q.enqueue( s )
    local found_path = false

    -- Iterate while not empty queue
    while (is_empty(Q) == false) do

        -- pop first
        local v = popright(Q)

        -- add neighbors
        local pushed = 0
        for _, w in ipairs(getNeighbours(v, dangerMap, step_size)) do
            if (visited[hashNode(w)] ~= 1) then
                -- if w is not visited
                visited[hashNode(w)] = 1 -- and mark it visited
                pushleft(Q, w) -- table.insert (Q, w)  --- enqueue w
                -- Spring.Echo("Pushed: [" .. w[1] .. "," .. w[2].. "]")
                pushed = pushed + 1
                predacessors[hashNode(w)] = v -- and save that "v" is predacessor of "w" ... v --> w
            end
        end
        -- Spring.Echo("Node[" .. v[1] .. "," .. v[2] .. "] Pushed neighbors: " .. pushed )

        -- end if result is found
        if (v[1] == end_node[1] and v[2] == end_node[2]) then
            found_path = true
            break
        end
    end
    -------------- analyze result
    if found_path == false then
        Spring.Echo("[sensor fail]: path not found.")
        return nil
    else
        Spring.Echo("[sensor success]: path found.")
    end
    return reconstruct_path(predacessors, start_node, end_node)
end

return function(dangerMap, assignments)
    for _, assignment in ipairs(assignments) do
        local unitID = assignment[1]
        local targetID = assignment[2]
        local XA, _, ZA = Spring.GetUnitPosition(unitID)
        local XB, _, ZB = Spring.GetUnitPosition(targetID)
        local foundPath = find_single_path(dangerMap, { XA, ZA }, { XB, ZB })
        if foundPath ~= nil then
            return { ["vehicleID"] = unitID, ["cargoID"] = targetID, ["path"] = foundPath }
        end
    end
    return nil
end