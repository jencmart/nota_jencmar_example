local sensorInfo = {
    name = "GetEnemyIDS",
    desc = "Get all IDS that belong to enemy teams.",
    author = "jencmar",
    date = "2021-06-28",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function tableContainsValue(table, value)
    for _, val in ipairs(table) do
        if val == value then
            return true
        end
    end
    return false
end

return function()
    -- un-synced
    --local myTeamID = Spring.GetMyTeamID()
    --local myPlayerID = Spring.GetMyPlayerID()
    --local myAllyID = Spring.GetMyAllyTeamID()

    -- synced
    --Spring.GetTeamList()     -- teams are the groups who control it's units
    --Spring.GetAllyTeamList() -- set of teams who share the same winning condition
    --Spring.GetPlayerList()   -- players are connections to hosts (something we don't need)

    local allTeams = Spring.GetTeamList()
    local alliesTeams = Spring.GetAllyTeamList()
    local myTeamID = Spring.GetMyTeamID()

    local enemyTeams = {}
    for _, teamID in pairs(allTeams) do
        --local _,_,_,isAITeam = Spring.GetTeamInfo(teamID)
        --local isLuaAI = (Spring.GetTeamLuaAI(teamID) ~= "")
        if tableContainsValue(alliesTeams, teamID) ~= false and teamID ~= myTeamID then
            enemyTeams[#enemyTeams + 1] = teamID
        end
    end

    return enemyTeams
end