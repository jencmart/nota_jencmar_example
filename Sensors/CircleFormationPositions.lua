local sensorInfo = {
    name = "CircleFormation",
    desc = "Creates uniformly distributed positions on the circle.",
    author = "jencmar",
    date = "2021-06-04",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local sin, cos = math.sin, math.cos


-- @description return positions for units to create circle
-- @argument cntUnits number of units in circle formation
return function(cntUnits)
    r = cntUnits * 5 + 100
    pi = 3.14
    vectorPositions = {}
    for i = 1, cntUnits do
        vectorPositions[i] = Vec3(r * cos(2 * r * pi / i), 0, r * sin(2 * r * pi / i)) -- positions are on circle
    end
    return vectorPositions
end