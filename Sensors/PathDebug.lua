local sensorInfo = {
    name = "PathDebug",
    desc = "Sends data debug widget",
    author = "jencmar",
    date = "2021-06-04",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local function computeGridStep(dm)
    items = 0
    for _, _ in pairs(dm) do
        items = items + 1
    end
    Spring.Echo("ITEMS" .. items)
    return Game.mapSizeX / (items - 1)
end

return function(pathToDraw)
    for _, poss in pairs(pathToDraw) do
        pos = poss:AsSpringVector()
        local grid_x = pos[1]
        local grid_y = pos[2]
        local grid_z = pos[3]
        STEP = 16
        z_min = math.max(grid_z - STEP, 0)
        z_max = math.min(grid_z + STEP, Game.mapSizeZ)
        idx = grid_x * Game.mapSizeX + grid_z

        Script.LuaUI.exampleDebug_update(idx, -- key
                {        -- data
                    startPos = Vec3(grid_x, grid_y + 50, z_min),
                    endPos = Vec3(grid_x, grid_y + 50, z_max),
                    color = { 0.2, 0.9, 0.1 },
                    line_width = 2
                }
        )
    end

end