local sensorInfo = {
    name = "AddEnemyUnits",
    desc = "Add all currently visible enemy units are stored.",
    author = "jencmar",
    date = "2021-06-13",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



--                                    maxRange   height LOS       airLOS
local range_data = { ["Crasher"] = { dist = 680, h = 21, los = 655, airLos = 982 },
                     ["DCA"] = { dist = 650, h = 18, los = 650, airLos = 975 },
                     ["Pulverizer"] = { dist = 1060, h = 71, los = 650, airLos = 975 }, -- corrl
                     ["Cobra"] = { dist = 1850, h = 35, los = 650, airLos = 1200 },
                     ["A.K"] = { dist = 360, h = 24, los = 461, airLos = 691 },
                     ["Thud"] = { dist = 590, h = 18, los = 430, airLos = 645 },
                     ["Horgue"] = { dist = 1000, h = 27, los = 700, airLos = 1050 },
                     ["Shamshir"] = { dist = 3500, h = 98, los = 1000, airLos = 1500 },
                     ["Slasher"] = { dist = 680, h = 19, los = 511, airLos = 766 },
                     ["Mandau"] = { dist = 600, h = 59, los = 700, airLos = 1050 },
                     ["Storm"] = { dist = 460, h = 18, los = 464, airLos = 696 },
                     ["Gaat Gun"] = { dist = 610, h = 70, los = 613, airLos = 919 },
                     ["Generator"] = { dist = 0, h = 114, los = 367, airLos = 550 }
}

local function tryToFindRangeData(tooltip)
    if tooltip ~= nil then
        for name, data in pairs(range_data) do
            if string.find(tooltip, name) then
                return data
            end
        end
    end
    return nil
end

local function setEnemy(enemyUnits, unitID)

    -- Not set ok / unknown ..
    local x, y, z = Spring.GetUnitPosition(unitID)

    local maxWeaponRange = Spring.GetUnitWeaponState(unitID, 1, "range") and Spring.GetUnitWeaponState(unitID, 1, "range") or 700
    local height = 50
    local los = 650
    local airLos = 800
    local name = nil
    local set = false

    local enemyName = Spring.GetUnitTooltip(unitID)
    local result = tryToFindRangeData(enemyName)
    if result ~= nil then
        maxWeaponRange = result.dist
        height = result.h
        los = result.los
        airLos = result.airLos
        name = enemyName
        set = true
    end

    enemyUnits[unitID] = { x, y, z, maxWeaponRange, height, los, airLos, name, set }
end

return function(enemyIDS, enemyUnits)

    -- init if new ...
    if (enemyUnits == nil) then
        enemyUnits = {}
    end

    -- for each enemy ID enemies ...
    for _, enemyID in pairs(enemyIDS) do
        -- for each unit of enemy ID ...
        foundUnits = Spring.GetTeamUnits(enemyID)
        for _, unitID in pairs(foundUnits) do
            -- Unknown unit or set to default
            if enemyUnits[unitID] == nil or enemyUnits[unitID][9] == false then
                setEnemy(enemyUnits, unitID)
            end
        end
    end

    return enemyUnits
end