local sensorInfo = {
    name = "AssignTrasports",
    desc = "Computes assignmnet between transporters and load.",
    author = "jencmar",
    date = "2021-06-13",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local function isAtlasUnit(unitDef)
    local TMP = UnitDefs[unitDef].name == "armatlas" -- UnitDefs[unitDef].isTransport

    --if TMP == true then
    --	Spring.Echo("IS TRANSPORT")
    --end

    return TMP
end

local function isTransportable(unitDef)
    local immobileOrPlane = UnitDefs[unitDef].isImmobile or UnitDefs[unitDef].name == "armpeep" or UnitDefs[unitDef].name == "armatlas"
    return immobileOrPlane == false
end

local function isInDanger(unitID, safeZone)
    local x, y, z = Spring.GetUnitPosition(unitID)
    local loadPos = Vec3(x, 0, z)
    local vv = safeZone.center
    local sx = vv.x
    local sy = vv.y
    local sz = vv.z
    local radius = safeZone.radius
    local zoneCenter = Vec3(sx, 0, sz)
    local dist = loadPos:Distance(zoneCenter)
    if (dist > radius) then
        return true
    end
    return false
end

-- GetUnitIsTransporting .. how many units is transporting 


-- armbox total points: 100
-- [f=0001043] Priority for: armbull total points: 6
-- [f=0001043] Priority for: armham total points: 13
-- [f=0001043] Priority for: armrock total points: 6
local function countAndOrderNotSafeUnits(units)
    -- Box of death - armbox      - 10pt  (10x) = 100
    -- Hatrack - armllr armllr2   - 5pt   (16x) = 80
    -- Bulldog - armbull          - 2pt    (6x) = 12
    -- Hammer - armham            - 1pt   (13x) = 13
    -- Rocko - armrock            - 1pt   (6x)  = 6
    local unit_priority = { { "armbox", 10 }, { "armmllt", 5 }, { "armmllt2", 5 }, { "armbull", 2 }, { "armham", 1 }, { "armrock", 1 } }

    local res = {}
    for _, unitID in ipairs(units) do
        local unitName = UnitDefs[Spring.GetUnitDefID(unitID)].name
        if res[unitName] == nil then
            res[unitName] = { unitID }
        else
            table.insert(res[unitName], unitID)
        end
    end

    -- Log
    --for unitName, unitIDs in ipairs(res) do
    --    Spring.Echo("Unit: " .. unitName .. " [" .. #unitIDs .. "]")
    --end

    -- Prioritize
    local units_with_priority = {}
    for _, data in ipairs(unit_priority) do
        name = data[1]
        points = data[2]
        if res[name] ~= nil then
            for _, id in ipairs(res[name]) do
                table.insert(units_with_priority, id)
            end
            --Spring.Echo("Priority for: " .. name .. " total points: " .. #res[name] * points)
        end

    end
    return units_with_priority
end

local function assignVehiclesAndCargo(safeZone)
    local emptyVehicles = {}
    local cargoInDanger = {}
    local allUnits = Spring.GetTeamUnits(Spring.GetMyTeamID()) -- 120 units

    -- for each MY unit
    for _, unitID in ipairs(allUnits) do
        local unitDef = Spring.GetUnitDefID(unitID)
        local isEmptyVehicle = isAtlasUnit(unitDef) and #Spring.GetUnitIsTransporting(unitID) == 0 or false -- not vehicle -> not empty

        if isEmptyVehicle and global["blockedUnits"][unitID] == nil then
            emptyVehicles[#emptyVehicles + 1] = unitID

        elseif isTransportable(unitDef) and isInDanger(unitID, safeZone) and  global["blockedUnits"][unitID] == nil then
            cargoInDanger[#cargoInDanger + 1] = unitID
        end
    end

    -- give prioritized pickup
    cargoInDanger = countAndOrderNotSafeUnits(cargoInDanger)
    -- assign transports to units
    local result = {}
    local curr_transp = 1
    for _, tranporterID in ipairs(emptyVehicles) do
        if cargoInDanger[curr_transp] == nil then
            break
        end
        result[#result + 1] = { tranporterID, cargoInDanger[curr_transp] }
        -- Spring.Echo("Assigning: " .. tranporterID .. " -> " ..  not_safe[curr_transp] )
        curr_transp = curr_transp + 1
    end

    return result
end

local function getRandomNumber(a, b)
    math.randomseed(os.time())
    math.random(); math.random(a, b); math.random(a, b)
    return math.random(a, b)
end

-- return   transportData
return function(safeZone)
    while global["locked"] do
        local start = os.clock()
        local duration =  getRandomNumber(1, 5)
        while os.clock() - start < duration do end
    end
    global["locked"] = true

    -- assign transports and cargo
    local allAssigns = assignVehiclesAndCargo(safeZone)

    -- find some viable path for (Vehicle, Cargo)
    local transportData = Sensors.nota_jencmar_example.FindSafePath(global["dangerMap"], allAssigns)

    if transportData ~= nil then
        -- block (Vehicle, Cargo)
        global["blockedUnits"][transportData.vehicleID] = true
        global["blockedUnits"][transportData.cargoID] = true
    end

    global["locked"] = nil

    return transportData
end
