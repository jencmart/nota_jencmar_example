nota_jencmar_example 0.3
====
*{behaviors for the HLAA 2021 assignments}*

Behaviors (Trees)
----

* sandSail
* ctp2
* ttdr
* analyzeMap
* moveSingleUnit

Sensors
----

* Wind
* CircleFormationPositions
* FindHillPositions
* BuildDangerMap
* FindSafePath
* AddEnemyUnits
* AssignTransports
* ReversePath

DebugUtils
----

* WindDirectionDebug
* DangerMapDebug
* PathDebug


Actions (Nodes / Commands)
----

* moveAround
* slidingEdge
* followPath
* manualEnd
* moveToEdge
* load
* unload

* [dependencies](./dependencies.json)
